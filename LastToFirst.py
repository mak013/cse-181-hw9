from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9k.txt"
WRITE_PATH = "Answers/rosalind_ba9k.txt"

def first_last_wrapper(Transform, ith):
	column = sorted(Transform)
	symbol = Transform[ith]
	return first_last(Transform, column, symbol, ith)

# create a wrapper around this called FirstLastWrapper
def first_last(Text, Column, symbol, ith):
	counter = 0
	# go through Text to check what ith repeated symbol it is
	for i in xrange(len(Text)):
		if Text[i] == symbol:
			counter += 1
		if i == ith:
			break
	# go through the column to check where this ith symbol is
	checker = 0
	for i in xrange(len(Column)):
		if Column[i] == symbol:
			checker += 1
		if checker == counter:
			return i

data = ReadFromFile(READ_PATH)
transform = data[0]
idx = data[1]

print first_last_wrapper(transform, int(idx))
from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9n.txt"
WRITE_PATH = "Answers/rosalind_ba9n.txt"

def MultiPatternMatching(Text, Patterns):
	locations = []

	suffix_array = SuffixArray(Text)
	last_col = create_last_column(Text)
	# for loop
	for Pattern in Patterns:
		top, bottom = BetterBWMatchingWrapper(last_col, Pattern)
		if top == -1:
			continue
		array = [suffix_array[i] for i in xrange(top, bottom+1)]
		for x in array:
			locations.append(x[1])
	return sorted(locations)

def SuffixArray(Text):
	suffix_array = []
	Text = Text + "$"
	for i in xrange(len(Text)):
		suffix_array.append((Text[i:],i))
	suffix_array = sorted(suffix_array)
	return suffix_array

def create_last_column(Text):
	Text = Text + "$"
	rotations = []
	for i in xrange(len(Text)):
		Text = Text[-1] + Text[:len(Text)-1]
		rotations.append(Text)
	last_column = ""
	rotations = sorted(rotations)
	for rotation in rotations:
		last_column += rotation[-1]
	return last_column

def BetterBWMatchingWrapper(Transform, Pattern):
	arr= []
	first_column = sorted(Transform)
	last_column = Transform
	return BetterBWMatching(first_column, last_column, Pattern)

def BetterBWMatching(firstCol, lastCol, Pattern):
	top = 0
	bottom = len(lastCol) -1
	while top <= bottom:
		if(len(Pattern) != 0):
			symbol = Pattern[-1]
			Pattern = Pattern[:len(Pattern)-1]
			if symbol in lastCol[top:bottom+1]:
				top = FirstOccurence(firstCol, symbol) + Count(symbol, top, lastCol)
				bottom = FirstOccurence(firstCol, symbol) + Count(symbol, bottom + 1, lastCol) - 1
			else:
				return -1,-1
		else:
			return top, bottom

def Count(Symbol, I, LastColumn):
	return LastColumn[:I].count(Symbol)
def FirstOccurence(FirstColumn, Symbol):
	for i in xrange(len(FirstColumn)):
		if FirstColumn[i] == Symbol:
			return i

data = ReadFromFile(READ_PATH)
text = data[0]
patterns = data[1:]

ans = MultiPatternMatching(text, patterns)

f = open(WRITE_PATH, "w")
for x in ans:
	f.write(str(x) + " ")
f.close()
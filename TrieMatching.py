from helpers import ReadFromFile, TrieConstruction

READ_PATH = "Problems/rosalind_ba9b.txt"
WRITE_PATH = "Answers/rosalind_ba9b.txt"

def PrefixTrieMatching(Text, Trie):
	# keep track of what next symbol to check if matches
	i = 0
	# first letter of text
	symbol = Text[0]
	# root
	v = 0
	print Text
	while True:
		edges_from_v = filter(lambda x: x[0]==v, Trie.keys())
		values_of_edges = [Trie[edge] for edge in edges_from_v]
		# check if v is a leaf in the trees
		incoming_nodes = [edge[0] for edge in Trie.keys()]
		if v not in incoming_nodes:
			return "match"
		# check if there is an edge labled by v,w with symbol
		elif symbol in values_of_edges:
			for edge in edges_from_v:
				if Trie[edge] == symbol:
					v = edge[1]
					i+=1
					if i >= len(Text):
						break
					else:
						symbol = Text[i]
						break
		else:
			return "no match"

def TrieMatching(Text, Trie):
	array = []
	for i in xrange(len(Text)):
		resp = PrefixTrieMatching(Text[i:], Trie)
		if resp == "match":
			array.append(i)
	return array

lines = ReadFromFile(READ_PATH)
Text = lines[0]
Patterns = lines[1:]
G = TrieConstruction(Patterns)

indices = TrieMatching(Text, G)

f = open(WRITE_PATH, "w")
token = ""
for x in indices:
	token += str(x) + " "
f.write(token[:len(token)-1])
f.close()
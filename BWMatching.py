from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9i.txt"
WRITE_PATH = "Answers/rosalind_ba9i.txt"

def BWMatching_Wrapper(Text, Patterns):
	arr= []
	first_column = sorted(Text)
	last_column = Text
	for Pattern in Patterns:
		arr.append(BWMatching(first_column, last_column, Pattern))
	return arr


def BWMatching(firstCol, lastCol, Pattern):

	top = 0
	bottom = len(lastCol) -1
	while top <= bottom:
		if(len(Pattern) != 0):
			symbol = Pattern[-1]
			Pattern = Pattern[:len(Pattern)-1]
			if symbol in lastCol[top:bottom+1]:
				# get the first position of the symbol from top to bottom
				topIdx = first_or_last_pos(top, bottom, lastCol, symbol, "first")
				botIdx = first_or_last_pos(top, bottom, lastCol, symbol, "last")
				top = first_last(lastCol, firstCol, symbol, topIdx)
				bottom = first_last(lastCol, firstCol, symbol, botIdx)

			else:
				return 0
		else:
			return bottom - top + 1

def first_last(Text, Column, symbol, ith):
	counter = 0
	# go through Text to check what ith repeated symbol it is
	for i in xrange(len(Text)):
		if Text[i] == symbol:
			counter += 1
		if i == ith:
			break
	# go through the column to check where this ith symbol is
	checker = 0
	for i in xrange(len(Column)):
		if Column[i] == symbol:
			checker += 1
		if checker == counter:
			return i

def first_or_last_pos(top, bottom, text, symbol, which):
	newText = text
	if which == "first":
		for i in xrange(len(newText)):
			if i >= top and i <= bottom and newText[i] == symbol:
				return i
	else:
		for i in xrange(len(newText)):
			j = len(newText) - i - 1
			if j >= top and j <= bottom and newText[j] == symbol:
				return j

data = ReadFromFile(READ_PATH)
text = data[0]
patterns = data[1].split()
ans = BWMatching_Wrapper(text, patterns)

f = open(WRITE_PATH, "w")
for x in ans:
	f.write(str(x) + " ")
f.close()


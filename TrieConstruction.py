from helpers import ReadFromFile
from collections import defaultdict

READ_PATH = "Problems/rosalind_ba9a.txt"
WRITE_PATH = "Answers/rosalind_ba9a.txt"

def TrieConstruction(patterns):
	g = defaultdict(list)
	max_node = 0

	for pattern in patterns:
		current_node = 0
		for c in pattern:
			# check if edge from current_node exist with c weight
			edges_from_current = filter(lambda x: x[0] == current_node, g.keys())
			edge_values_from_current = [g[edge] for edge in edges_from_current]

			if c in edge_values_from_current:
				for edge in edges_from_current:
					if g[edge] == c:
						current_node = edge[1]
						break
			else:
				max_node += 1
				g[(current_node, max_node)] = c
				current_node = max_node
	return g


patterns = ReadFromFile(READ_PATH)
g = TrieConstruction(patterns)

# write the output to the file
f = open(WRITE_PATH, "w")
for edge in g.keys():
	token = str(edge[0]) + "->" + str(edge[1]) + ":" + g[edge] + "\n"
	f.write(token)
f.close()
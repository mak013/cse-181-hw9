from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9j.txt"
WRITE_PATH = "Answers/rosalind_ba9j.txt"

def InverseBWT(Transform):
	# create a lexicographic order of the Transform
	first_column = sorted(Transform)
	# text to reconstruct from transform
	Text = "$"
	# use the the first-last rule to reconstruct our string
	first = 0
	last = 0
	while len(Text) != len(Transform):
		first_symbol = first_column[first]
		last = first_last(first_column, Transform, first_symbol, first)
		first_symbol = first_column[last]
		Text += first_symbol
		first = last
		print Text
	return Text[1:] + "$"

def first_last(Text, Column, symbol, ith):
	counter = 0
	# go through Text to check what ith repeated symbol it is
	for i in xrange(len(Text)):
		if Text[i] == symbol:
			counter += 1
		if i == ith:
			break
	# go through the column to check where this ith symbol is
	checker = 0
	for i in xrange(len(Column)):
		if Column[i] == symbol:
			checker += 1
		if checker == counter:
			return i

transform = ReadFromFile(READ_PATH)[0]
inverse = InverseBWT(transform)

f = open(WRITE_PATH, "w")
f.write(inverse)
f.close()
from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9m.txt"
WRITE_PATH = "Answers/rosalind_ba9m.txt"

def BetterBWMatchingWrapper(Transform, Patterns):
	arr= []
	first_column = sorted(Transform)
	last_column = Transform
	for Pattern in Patterns:
		arr.append(BetterBWMatching(first_column, last_column, Pattern))
	return arr

def BetterBWMatching(firstCol, lastCol, Pattern):
	top = 0
	bottom = len(lastCol) -1
	while top <= bottom:
		if(len(Pattern) != 0):
			symbol = Pattern[-1]
			Pattern = Pattern[:len(Pattern)-1]
			if symbol in lastCol[top:bottom+1]:
				top = FirstOccurence(firstCol, symbol) + Count(symbol, top, lastCol)
				bottom = FirstOccurence(firstCol, symbol) + Count(symbol, bottom + 1, lastCol) - 1

			else:
				return 0
		else:
			return bottom - top + 1

def Count(Symbol, I, LastColumn):
	return LastColumn[:I].count(Symbol)
def FirstOccurence(FirstColumn, Symbol):
	for i in xrange(len(FirstColumn)):
		if FirstColumn[i] == Symbol:
			return i

data = ReadFromFile(READ_PATH)
transform = data[0]
patterns = data[1].split()

ans = BetterBWMatchingWrapper(transform, patterns)

f = open(WRITE_PATH, "w")
for x in ans:
	f.write(str(x) + " ")
f.close()


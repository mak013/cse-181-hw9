from helpers import ReadFromFile

READ_PATH = "Problems/rosalind_ba9q.txt"
WRITE_PATH = "Answers/rosalind_ba9q.txt"

def PartialSuffixArrayConstruction(Text, K):
	suffix_arr = []
	for i in xrange(len(Text)):
		suffix_arr.append((Text[i:],i))
	suffix_arr = sorted(suffix_arr)

	partial_suffix_arr = []
	for x in xrange(len(suffix_arr)):
		if suffix_arr[x][1]%k == 0:
			partial_suffix_arr.append((x,suffix_arr[x][1]))
	return partial_suffix_arr


data = ReadFromFile(READ_PATH)
text = data[0]
k = int(data[1])

ans = PartialSuffixArrayConstruction(text, k)

f = open(WRITE_PATH, "w")
for x in ans:
	f.write(str(x[0]) + "," + str(x[1]) + "\n")
f.close()